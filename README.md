# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is an application that lets users manage departments and employees

### How do I get set up? ###

### System Requirements
 - Appication requires PHP 5.3.2+ to run.
 - Composer 
 
### Login Info
 - Username: admin@gmail.com
 - Password: 123123 
 
### Database
 - database/parkwaylabs.sql 


### Installation instructions

- Application utilizes *Composer* to manage its dependencies. So, before using the application, make sure you have Composer installed on your machine.
- All of the configuration files for the application are stored in the config directory. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact