@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   <div class="col-md-3"><i class="fa fa-building" aria-hidden="true"></i> <a href="<?= route('department.index') ?>">Manage Departments</a></div>
                   <div class="col-md-3"><i class="fa fa-users" aria-hidden="true"></i> <a href="<?= route('employee.index') ?>">Manage Employees</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
