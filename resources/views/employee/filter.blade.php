
    @if($employees)
        <table class="table table-responsive table-bordered col-md-12">
            <thead>
            <tr>
                <th>EMP No</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Department</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees AS $employee)
                <tr>
                    <td><?= $employee->emp_no ?></td>
                    <td><?= $employee->fname ?></td>
                    <td><?= $employee->lname ?></td>
                    <td><?= $employee->address ?></td>
                    <td><?= $employee->department->name ?></td>
                    <td><?= ($employee->status)?'Active':'Inactive' ?></td>
                    <td align="center"><a href="<?= route('employee.edit',['id'=>$employee->id]) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No employees found</p>
    @endif
