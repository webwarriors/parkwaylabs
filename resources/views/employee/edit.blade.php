@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Employees Manager - Update Employee</div>
                    {!! Form::model($employee, ['method' => 'PATCH', 'route' => ['employee.update',  $employee->id]]) !!}
                    <div class="panel-body">
                        @if (Session::has('message'))
                            @if(Session::get('message') == 'update')
                                <label  class="alert alert-success">
                                    Employee successfully updated.
                                </label>
                            @endif
                        @endif
                        @include('employee._form')
                            @if($employee->id)
                                <div class="form-group {!! ($errors->has('status')) ? 'has-error' : '' !!}">
                                    <span class="required">*</span>{!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
                                    {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'], null,['class' => 'form-control',]) !!}
                                    <span class="help-block">{!! ($errors->has('status') ? $errors->first('status') : '') !!}</span>
                                </div>
                            @endif
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
                        {!! link_to_route('employee.index', 'Cancel', [ ], ['class' => 'btn btn-default']) !!}
                    </div>
                    {!! Form::close()  !!}
                </div>
            </div>
        </div>
@endsection
