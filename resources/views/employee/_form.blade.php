@inject('departments', 'App\Http\Utilities\Departments')

<div class="form-group {!! ($errors->has('fname')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('emp_no', 'EMP No', ['class' => 'control-label']) !!}
    {!! Form::text('emp_no', null, ['class' => 'form-control', 'placeholder' => 'EMP No',]) !!}
    <span class="help-block">{!! ($errors->has('emp_no') ? $errors->first('emp_no') : '') !!}</span>
</div>
<div class="form-group {!! ($errors->has('department_id')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('department_id', 'Department', ['class' => 'control-label']) !!}
    {!! Form::select('department_id', $departments->selectDeptOptions(), null,['class' => 'form-control',]) !!}
    <span class="help-block">{!! ($errors->has('department_id') ? $errors->first('department_id') : '') !!}</span>
</div>
<div class="form-group {!! ($errors->has('fname')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('fname', 'First Name', ['class' => 'control-label']) !!}
    {!! Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'First Name',]) !!}
    <span class="help-block">{!! ($errors->has('fname') ? $errors->first('fname') : '') !!}</span>
</div>
<div class="form-group {!! ($errors->has('lname')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('lname', 'Last Name', ['class' => 'control-label']) !!}
    {!! Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Last Name',]) !!}
    <span class="help-block">{!! ($errors->has('lname') ? $errors->first('lname') : '') !!}</span>
</div>
<div class="form-group {!! ($errors->has('address')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address',]) !!}
    <span class="help-block">{!! ($errors->has('address') ? $errors->first('address') : '') !!}</span>
</div>
<div class="form-group {!! ($errors->has('designation')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('designation', 'Designation', ['class' => 'control-label']) !!}
    {!! Form::text('designation', null, ['class' => 'form-control', 'placeholder' => 'Designation',]) !!}
    <span class="help-block">{!! ($errors->has('designation') ? $errors->first('designation') : '') !!}</span>
</div>


