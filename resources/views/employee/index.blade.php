@extends('layouts.app')
@inject('departments', 'App\Http\Utilities\Departments')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Employees Manager</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">

                                {!! link_to_route('employee.create' , '+ Add new employee', [], ['class' => 'btn btn-primary btn-create']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 right"><div class="form-group {!! ($errors->has('department_id')) ? 'has-error' : '' !!}">
                                    {!! Form::label('department_id', 'Department', ['class' => 'control-label']) !!}
                                    {!! Form::select('department_id', $departments->selectDeptOptions(), null,['class' => 'form-control','onChange'=>'filterRows()']) !!}
                                    <span class="help-block">{!! ($errors->has('department_id') ? $errors->first('department_id') : '') !!}</span>
                                </div></div>
                            <div class="col-md-12" id="emp-data">
                            @if($employees)
                                <table class="table table-responsive table-bordered  col-md-12">
                                    <thead>
                                        <tr>
                                            <th>EMP No</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Address</th>
                                            <th>Department</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($employees AS $employee)
                                       <tr>
                                           <td><?= $employee->emp_no ?></td>
                                           <td><?= $employee->fname ?></td>
                                           <td><?= $employee->lname ?></td>
                                           <td><?= $employee->address ?></td>
                                           <td><?= $employee->department->name ?></td>
                                           <td><?= ($employee->status)?'Active':'Inactive' ?></td>
                                           <td align="center"><a href="<?= route('employee.edit',['id'=>$employee->id]) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                       </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>No employees found</p>
                            @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>

    function filterRows(){

        var dept = $('select#department_id option:selected').val();

        $.ajax({
            url: "/employee",
            data: { id: dept}
        }).done(function( data ) {
          $('#emp-data').html(data);
        });

    }

</script>