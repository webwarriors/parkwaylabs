@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Departments Manage - Create New Department</div>
                {!!  Form::open(['route' => ['department.store']]) !!}
                <div class="panel-body">
                    @if (Session::has('message'))
                        @if(Session::get('message') == 'create')
                            <label  class="alert alert-success">
                                Department successfully added.
                            </label>
                        @endif
                    @endif
                    @include('department._form')
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
                    {!! link_to_route('department.index', 'Cancel', [ ], ['class' => 'btn btn-default']) !!}
                </div>
                {!! Form::close()  !!}
            </div>
        </div>
    </div>
@endsection
