@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Departments Manager</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">

                                {!! link_to_route('department.create' , '+ Add new department', [], ['class' => 'btn btn-primary btn-create']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                            @if($departments)
                                <ul>
                                    @foreach($departments AS $department) {{-- Main departments --}}
                                       <li><?= $department->name ?> <label><a href="<?= route('department.edit',['id'=>$department->id]) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></label></li>
                                        @if(!empty($department->subDepartments()->get()))
                                            <ul>
                                            <?php $sub_depts = $department->subDepartments()->get(); ?>
                                            @foreach($sub_depts AS $sub_dept) {{-- Sub departments --}}
                                                <li><?= $sub_dept->name; ?> <label><a href="<?= route('department.edit',['id'=>$sub_dept->id]) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></label></li>
                                            @endforeach
                                            </ul>
                                        @endif
                                    @endforeach
                                </ul>
                            @else
                                <p>No departments found</p>
                            @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
