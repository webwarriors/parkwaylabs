@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Departments Manager - Update Department</div>

                    {!! Form::model($department, ['method' => 'PATCH', 'route' => ['department.update',  $department->id]]) !!}
                    <div class="panel-body">
                            @if (Session::has('message'))
                                @if(Session::get('message') == 'update')
                                    <label  class="alert alert-success">
                                        Department successfully updated.
                                    </label>
                                @endif
                            @endif
                        @include('department._form')
                        @if($department->id)
                            <div class="form-group {!! ($errors->has('status')) ? 'has-error' : '' !!}">
                                <span class="required">*</span>{!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
                                {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'], null,['class' => 'form-control',]) !!}
                                <span class="help-block">{!! ($errors->has('status') ? $errors->first('status') : '') !!}</span>
                            </div>
                        @endif
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
                        {!! link_to_route('department.index', 'Cancel', [ ], ['class' => 'btn btn-default']) !!}
                    </div>
                    {!! Form::close()  !!}
                </div>
            </div>
        </div>
@endsection
