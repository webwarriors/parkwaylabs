@inject('departments', 'App\Http\Utilities\Departments')
<div class="form-group {!! ($errors->has('title')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('main_dept_id', 'Main Department', ['class' => 'control-label']) !!}
    {!! Form::select('main_dept_id', $departments->selectOptions(), null,['class' => 'form-control',]) !!}
    <span class="help-block">{!! ($errors->has('main_dept_id') ? $errors->first('main_dept_id') : '') !!}</span>
</div>
<div class="form-group {!! ($errors->has('name')) ? 'has-error' : '' !!}">
    <span class="required">*</span>{!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name',]) !!}
    <span class="help-block">{!! ($errors->has('name') ? $errors->first('name') : '') !!}</span>
</div>

