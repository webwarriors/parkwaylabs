<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';

    protected $fillable = ['name', 'main_dept_id', 'status'];

    public function employees()
    {
        return $this->hasMany(Employee::class,'department_id');
    }

    public function subDepartments()
    {
        return $this->hasMany(Department::class, 'main_dept_id','id');
    }


}
