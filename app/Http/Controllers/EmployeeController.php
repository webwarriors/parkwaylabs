<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Illuminate\Support\Facades\Session;

class EmployeeController extends Controller
{
    protected $employee;
    protected $request;

    /**
     * EmployeeController constructor.
     * @param Employee $employee
     */
    public function __construct(Employee $employee, Request $request)
    {
        $this->middleware('auth');
        $this->employee = $employee;
        $this->request = $request;
    }


    /**
     * employee landing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $employees = $this->employee->all();

        if ($this->request->ajax()) {
            if ($this->request->has('id')) {

                $employees = $this->employee->where(['department_id'=>$this->request->get('id')])->get();


            }
            return view('employee.filter', compact('employees'));
        }

        return view('employee.index', compact('employees'));
    }

    public function create()
    {

        return view('employee.create');
    }

    /**
     * create new employee
     * @param EmployeeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(EmployeeRequest $request)
    {
        $employee = $this->employee;
        $employee->emp_no = $request->get('emp_no');
        $employee->fname = $request->get('fname');
        $employee->lname = $request->get('lname');
        $employee->department_id = $request->get('department_id');
        $employee->address = $request->get('address');
        $employee->designation = $request->get('designation');
        $result = $employee->save();

        if ($result) {
            Session::flash('message', "create");
        }
        return redirect(route('employee.create'));
    }

    public function edit($id)
    {
        $employee = $this->employee->findOrFail($id);
        return view('employee.edit', compact('employee'));
    }

    /**
     * Update employee
     * @param EmployeeRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(EmployeeRequest $request, $id)
    {
        $employee = $this->employee->findOrFail($id);
        $employee->emp_no = $request->get('emp_no');
        $employee->fname = $request->get('fname');
        $employee->lname = $request->get('lname');
        $employee->department_id = $request->get('department_id');
        $employee->address = $request->get('address');
        $employee->designation = $request->get('designation');
        $employee->status = $request->get('status');
        $result = $employee->save();
        if ($result) {
            Session::flash('message', "update");
        }

        return view('employee.edit', compact('employee'));
    }



}
