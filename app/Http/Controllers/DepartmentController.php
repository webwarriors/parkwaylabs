<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests\DepartmentRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{
    protected $department;

    /**
     * DepartmentController constructor.
     * @param Department $department
     */
    public function __construct(Department $department)
    {
        $this->middleware('auth');
        $this->department = $department;
    }

    /**
     * Department index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $departments = $this->department->where(['main_dept_id' => 0,'status'=>'1'])->get();
        return view('department.index', compact('departments'));
    }


    /**
     * Create department
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $departments = $this->department->where(['status' => 1])->lists('name', 'id')->toArray();
        $departments[0] = 'Main';
        //arsort($departments);
        return view('department.create', compact('departments'));
    }

    /**
     * Save department
     * @param DepartmentRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(DepartmentRequest $request)
    {

        $department = $this->department;
        $department->status = 1;
        $department->main_dept_id = $request->get('main_dept_id');
        $department->name = $request->get('name');
        $result = $department->save();
        if($result){
            Session::flash('message', "create");
        }
        return redirect(route('department.create'));

    }

    /**
     * Edit department
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){

        $department = $this->department->findOrFail($id);
        return view('department.edit', compact('department'));

    }

    /**
     * @param DepartmentRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(DepartmentRequest $request, $id)
    {

        $department = $this->department->findOrFail($id);
        $department->status = $request->get('status');;
        $department->main_dept_id = $request->get('main_dept_id');
        $department->name = $request->get('name');
        $result = $department->save();
        if($result){
            Session::flash('message', "update");
        }

        return view('department.edit', compact('department'));


    }


}
