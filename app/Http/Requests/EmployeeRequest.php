<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emp_no'=>'required|unique:employee,emp_no,'.$this->employee,
            'fname'=>'required',
            'lname'=>'required',
            'address'=>'required',
            'department_id'=>'required',
            'designation'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'fname.required' => 'The first name field is required.',
            'lname.required'  => 'The last name field is required.',
        ];
    }
}
