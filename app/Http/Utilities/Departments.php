<?php

namespace App\Http\Utilities;

use App\Department;

class Departments
{

    public static function selectOptions()
    {
        $dept = Department::where(['status'=>1])->lists('name','id')->toArray();
        $dept[0] = "Main";
        ksort($dept);
        return $dept;
    }

    public static function selectDeptOptions()
    {
        $dept = Department::where(['status'=>1])->lists('name','id')->toArray();
        ksort($dept);
        return $dept;
    }


}