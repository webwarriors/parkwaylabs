<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';

    protected $fillable = ['name', 'department_id', 'fname', 'lname', 'address', 'designation', 'status'];

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }

}
